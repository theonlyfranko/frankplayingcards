
// Lab Exercise 2 - Playing Cards
// Frank Ontaneda


/*
1. Part 1:
	A. Create a program that has a structure for playing cards.
		1. Call the struct "Card".
		2. The rank and suit should be enumerations. (Ace is high, value of 14)
	B. Create a PUBLIC repository and upload your project to Bitbucket.
	C. Notify your instructor when your project is uploaded.
2. Part 2: In-Lab Stuff
	A. Fork your partner's repository and add the following functions:
		1. void PrintCard (Card card)
			- Prints out the rank and suit of the card. Ex: The Queen of Diamonds
		2. Card HighCard(Card card1, Card card2)
			- Determines which of the two supplied cards has the higher rank.
	B. Commit and push your changes to Bitbucket.
3. Submit the link to your repository in Blackboard.
*/

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	HEARTS,
	CLUBS,
	DIAMONDS,
	SPADES
};

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card
{
	Rank rank;
	Suit suit;
};


void PrintCard(Card card)
{	
	Rank printRank = card.rank;
	Suit printSuit = card.suit;

	cout << "the ";

	switch (printRank)
	{
		case TWO: cout << "TWO"; break;
		case THREE: cout << "THREE"; break;
		case FOUR: cout << "FOUR"; break;
		case FIVE: cout << "FIVE"; break;
		case SIX: cout << "SIX"; break;
		case SEVEN: cout << "SEVEN"; break;
		case EIGHT: cout << "EIGHT"; break;
		case NINE: cout << "NINE"; break;
		case TEN: cout << "TEN"; break;
		case JACK: cout << "JACK"; break;
		case QUEEN: cout << "QUEEN"; break;
		case KING: cout << "KING"; break;
		case ACE: cout << "ACE"; break;
		default: cout << "Please enter a correct card rank.";
	}

	cout << " of ";

	switch (printSuit)
	{
		case CLUBS: cout << "CLUBS"; break;
		case DIAMONDS: cout << "DIAMONDS"; break;
		case HEARTS: cout << "HEARTS"; break;
		case SPADES: cout << "SPADES"; break;
		default: cout << "Please enter a correct card suit.";
	}

	cout << "." << endl;
}


Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		cout << "The high card is ";
		PrintCard(card1);
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		cout << "The high card is ";
		PrintCard(card2);
		return card2;
	}
	else
	{
		if (card1.suit > card2.suit)
		{
			cout << "The high card is ";
			PrintCard(card1);
			return card1;
		}
		else if (card1.suit < card2.suit)
		{
			cout << "The high card is ";
			PrintCard(card2);
			return card2;
		}
		else
		{
			cout << "The cards are the same suit and rank." << endl;
			return card1;		// You're just returning card1 here just to be sure to return something that is correct.
		}
	}
}


int main()
{
	Card card1;
	Card card2;

	card1.rank = NINE;
	card1.suit = CLUBS;

	card2.rank = QUEEN;
	card2.suit = HEARTS;

	PrintCard(card1);
	PrintCard(card2);
	cout << endl;
	cout << endl;
	cout << "It's over Anakin, I have the high card!" << endl;
	HighCard(card1, card2);


	_getch();
	return 0;
}